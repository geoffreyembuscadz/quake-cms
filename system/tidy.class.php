<?php
   class HTMLTidy {
      private $tidy;
      private $tidycfg = array();
      public  $content;
      
      public function Tidy($content) {
         if($content) {
            // Create an instance of the tidy class
            $this->tidy = new tidy();

            // Run tidy
            $this->tidycfg = array(
               'bare'             => true,
               'doctype'          => '-//W3C//DTD XHTML 1.0 Transitional//EN',
               'drop-empty-paras' => true,
               'drop-proprietary-attributes' => true,
               'enclose-text'     => true,
               'join-classes'     => true,
               'join-styles'      => true,
               'logical-emphasis' => true,
               'lower-literals'   => true,
               'merge-divs'       => true,
               'output-xhtml'     => true,
               'quote-ampersand'  => true,
               'quote-marks'      => true,                   
               'show-body-only'   => true,
               'word-2000'        => true
            );

            // Repair content input with tidy
            $this->content = $this->tidy->repairString($content, $this->tidycfg);
            
            return true;
         } else {
            return false;
         }
      }
   }
?>
