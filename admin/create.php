<?php if(isset($_SESSION['admin'])) { ?>
   <h3>Create a page</h3>
   <form id="create-page">
      <table>
         <tr>
            <td><label for="slug">Page Slug:</label></td>
            <td align="left">
               <input type="text" name="slug" id="slug" />
               <label class="error" for="slug" id="slug_error">Slug is required.</label>
            </td>
         </tr>
         <tr>
            <td><label for="title">Page Title:</label></td>
            <td>
               <input type="text" name="title" id="title" />
               <label class="error" for="title" id="title_error">Title is required.</label>
            </td>
         </tr>
         <tr>
            <td><label for="keywords">Meta Keywords:</label></td>
            <td><input type="text" name="keywords" id="keywords" /></td>
         </tr>
         <tr>
            <td><label for="description">Meta Description:</label></td>
            <td><input type="text" name="description" id="description" /></td>
         </tr>
         <tr>
            <td><label for="menu_title">Menu Title:</label></td>
            <td>
               <input type="text"  name="menu_title" id="menu_title" />
               <label class="error" for="menu_title" id="menu_title_error">Menu Title is required.</label>
            </td>
         </tr>
         <tr>
            <td><label for="heading">Page Heading:</label></td>
            <td><input type="text" name="heading" id="heading" /></td>
         </tr>
      </table>
      <table>
         <tr>
            <td><label for="content">Content:</label></td>
         </tr>
         <tr>
            <td><textarea name="content" id="content" rows="10" cols="31"></textarea></td>
         </tr>
      </table>
      <table>
         <tr>
            <td><label for="status">Status:</label></td>
            <td>
               <select name="status" id="status">
                  <option value="0">Draft</option>
                  <option value="1">Hidden</option>
                  <option value="2" selected="selected">Published</option>
               </select>
            </td>
         </tr>
         <tr>
            <td><label for="order">Sort Order:</label></td>
            <td><input type="text" name="order" id="order" size="1" value="0" /></td>
         </tr>
         <tr>
            <td colspan="2"><a href="#" name="submit" class="submit buttons">Create it!</a></td>
         </tr>
      </table>
   </form>
   <script type="text/javascript">
      $(function() {
         $('.error').hide();

         function check_slug() {
            $('.error').hide();
            var slug = $("input#slug").val();

            $.post("validate.php", { slug: slug },
            function(result) {
               if(result == 1) {
                  return false;
               } else {
                  $('#slug_error').html('This slug is unavailable!');
                  $("label#slug_error").show();
                  return true;
               }  
            });
         }
         
         $("input#slug").blur(function() {
            check_slug();
         });
         
         $(".submit").click(function() {
            $('.error').hide();

            var slug = $("input#slug").val();
            var title = $("input#title").val();
            var menu_title = $("input#menu_title").val();

            if (slug == "" || check_slug()) {
               if(check_slug()) {
                  $('#slug_error').html('This slug is unavailable.');
               } else {
                  $('#slug_error').html('Slug is required.');
               }

               $("input#slug").focus();
               $("label#slug_error").show();
               return false;
            }

            if (title == "") {
               $("label#title_error").show();
               $("input#title").focus();
               return false;
            }

            if (menu_title == "") {
               $("label#menu_title_error").show();
               $("input#menu_title").focus();
               return false;
            }

            tinyMCE.triggerSave();

            $.ajax({
               type: "POST",
               url: location.href,
               data: $('#create-page').serialize()
            });

            window.location.replace("index.php?page=pages");
            return false; 
         });  
      });
   </script>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>
