<?php if(isset($_SESSION['admin'])) { ?>  
<?php
   $extensions = glob(EXTPATH . DS . '*', GLOB_ONLYDIR);
   $i = 0;
   foreach($extensions as $ext) {
      if(file_exists($ext . DS . 'index.php')) {
         $ext = str_replace(EXTPATH . DS, '', $ext);
         $extuc = ucwords(str_replace('_', ' ', $ext));
         $i++;
      }
      
      if($i > 0) { ?>
         <a href="index.php?page=extensions&ext=<?php echo $ext; ?>"><?php echo $extuc; ?></a>
<?php } ?>
<?php
   } // End foreach
   
   if($i == 0) {
      echo '<h3>No extensions installed.</h3>';
   }
?>
<?php } else { ?>
   <h3>You do not have permission to access this page!</h3>
<?php } ?>