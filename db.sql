-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2012 at 04:09 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fob`
--

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `menu_title` varchar(30) NOT NULL,
  `heading` varchar(50) DEFAULT NULL,
  `content` text,
  `status` tinyint(1) NOT NULL,
  `parent` varchar(100) DEFAULT NULL,
  `order` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(35) NOT NULL,
  `modified_by` varchar(35) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slug`, `title`, `keywords`, `description`, `menu_title`, `heading`, `content`, `status`, `parent`, `order`, `created_by`, `modified_by`, `creation_date`, `modified_date`) VALUES
(1, 'index', 'Home', 'quake cms, home, index', 'Quake CMS index page meta description', 'Home', 'Welcome to Quake CMS!', '<p>Dear Bosch,</p>\r\n<p>If you&#39;re reading this, it&#39;s already too late.</p>\r\n<p>Just kidding! If you&#39;re reading this, that means you&#39;ve\r\nsuccessfully installed <em>Quake CMS</em>! Congrats!</p>\r\n<hr />\r\n<h3>What Now?</h3>\r\n<p>Go check out the <a href="admin">admin</a> area to edit this and\r\nthe other pages, or create new ones!</p>\r\n<p><strong>Username:</strong> admin<br />\r\n<strong>Password:</strong> password</p>\r\n<p>Keep in mind - <strong><em>Quake CMS</em> is still in pre\r\nalpha</strong>, so there are still some features that are\r\nincomplete. Don&#39;t fret though - I am updating it almost daily,\r\nso keep an eye on the Git repository at <a href=\r\n"https://bitbucket.org/Clowerweb/quake-cms/downloads">https://bitbucket.org/Clowerweb/quake-cms/downloads</a>\r\n. You can also help me out by <a href=\r\n"https://bitbucket.org/Clowerweb/quake-cms/fork">creating your own\r\nfork</a> of <em>Quake CMS</em> and helping with the\r\ndevelopment!</p>\r\n\r', 2, NULL, 0, '', '', '2012-01-03 05:42:35', '2012-02-01 04:05:03'),
(2, '404', 'Not Found', 'Page Not Found', '404, Page not found', '404', 'Not found', '<p>The page you requested could not be found (Error 404).</p>\r\n\r', 1, NULL, 0, '', '', '2012-01-23 07:08:22', '2012-01-23 07:09:04'),
(3, 'about-us', 'About Quake CMS', 'about, us', 'About Quake CMS', 'About', 'About Quake CMS', '<h2 id="!what-is-quake-cms" style=\r\n"margin-top: 6px; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-weight: normal; font-size: 18px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 24px; color: #666666; border-width: 0px;">\r\nWhat is Quake CMS?</h2>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nQuake CMS is a free and open source object-oriented PHP and MySQL\r\ncontent management system currently in development by Clowerweb.\r\nThe goal of Quake is to be extremely lightweight, with only the\r\ncore features needed for users to create and manage a basic\r\nwebsite, and none of the added bloat that most other CMS systems\r\nseems to have.</p>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nQuake CMS is currently in <strong style=\r\n"border-style: initial; border-color: initial; border-image: initial; font-weight: bold; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px; margin: 0px;">\r\nPRE-ALPHA</strong> and should <strong style=\r\n"border-style: initial; border-color: initial; border-image: initial; font-weight: bold; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px; margin: 0px;">\r\nNOT</strong> be used for production websites, there are still some\r\nincomplete features, and it has not been tested for security. You\r\nare welcome to download the code and improve it any way you like,\r\nbut please submit your improvements to us.</p>\r\n<h2 id="!why-yet-another-cms" style=\r\n"margin-top: 6px; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-weight: normal; font-size: 18px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 24px; color: #666666; border-width: 0px;">\r\nWhy Yet ANOTHER CMS?</h2>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nThe aim of Quake CMS is to not be like the others. Here are the\r\nfeatures:</p>\r\n<ul style=\r\n"padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 36px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; list-style-type: none; list-style-position: initial; list-style-image: initial; color: #393939; line-height: 18px; border-width: 0px; margin: 0px;">\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nCreate, edit, and delete pages</li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nCreate, edit, and delete other admin users (coming soon)</li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nCreate multiple templates with ease, and switch between them with a\r\nfew clicks</li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nSet your site preferences (site name, slogan, time zone, template,\r\netc.)</li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nView site traffic statistics (coming soon)</li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nEasily develop your own mods or modules without editing any core\r\nfiles (coming soon)</li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nWYSIWYG editor for you or your clients, sporting <strong style=\r\n"border-style: initial; border-color: initial; border-image: initial; font-weight: bold; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px; margin: 0px;">\r\nfull live syntax highlighting in the HTML editor, with code\r\nhinting, line numbering, and find/replace</strong></li>\r\n<li style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px;">\r\nThat&#39;s it! It&#39;s that simple and basic</li>\r\n</ul>\r\n<h2 id="!who-is-quake-cms-for" style=\r\n"margin-top: 6px; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-weight: normal; font-size: 18px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 24px; color: #666666; border-width: 0px;">\r\nWho is Quake CMS for?</h2>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nQuake CMS is currently aimed at web developers who want to provide\r\ntheir clients with a very easy to use, simplified CMS solution. If\r\nyour client wants a basic 5-page website with the ability to modify\r\ntheir own content, why would you need something complicated, like\r\nWordpress or Drupal? These solutions can be much too complex - for\r\nboth the client and the developer - for what should be a simple\r\nproject.</p>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nIf you need to develop one of those 5-page &quot;mom and pop&quot;\r\nwebsites and you&#39;d like to offer your clients the ability to\r\nmodify their own content as easily as possible, and you want to do\r\nit all without the need to add extra time to your development\r\nschedule, then Quake CMS is for you.</p>\r\n<h2 id="!who-quake-cms-isnt-for" style=\r\n"margin-top: 6px; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-weight: normal; font-size: 18px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 24px; color: #666666; border-width: 0px;">\r\nWho Quake CMS ISN&#39;T for</h2>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nQuake CMS is <strong style=\r\n"border-style: initial; border-color: initial; border-image: initial; font-weight: bold; font-style: inherit; font-family: inherit; vertical-align: baseline; border-width: 0px; padding: 0px; margin: 0px;">\r\nnot</strong> designed for those needing more complex website\r\nsolutions, such as: blogs, ecommerce functionality, everyday\r\nvisitor registration, visitor page/post commenting, forums, or\r\nanything like that. If you have a large project, that&#39;s a job\r\nfor the big dog CMS systems. Quake CMS is mostly just simple page\r\nmanagement.</p>\r\n<h2 id="!sounds-awesome-where-can-i-get-it" style=\r\n"margin-top: 6px; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-weight: normal; font-size: 18px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 24px; color: #666666; border-width: 0px;">\r\nSounds Awesome! Where can I get it?</h2>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nYou can get it here on bitbucket (under the downloads tab), or\r\nclone it with Git, or visit the <a style=\r\n"border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-family: inherit; vertical-align: baseline; text-decoration: none; color: #2b547d; border-width: 0px; padding: 0px; margin: 0px;"\r\nhref="http://www.quakecms.com/">official Quake CMS website</a>.</p>\r\n<h2 id="!how-can-i-contribute-to-the-project" style=\r\n"margin-top: 6px; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; padding-top: 10px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-weight: normal; font-size: 18px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 24px; color: #666666; border-width: 0px;">\r\nHow can I Contribute to the Project?</h2>\r\n<p style=\r\n"margin-top: 9px; margin-right: 0px; margin-bottom: 9px; margin-left: 0px; border-style: initial; border-color: initial; border-image: initial; font-size: 13px; font-family: Helvetica, Arial, sans-serif; vertical-align: baseline; line-height: 18px; color: #393939; border-width: 0px; padding: 0px;">\r\nEither fork us here and I might merge some of your\r\nchanges/additions if they align with the spirit of the project, or\r\nyou can email me at chris {at} clowerweb {dot} com (obscured so\r\nspam bots (hopefully) don&#39;t pick up the address). Of course, if\r\nyou&#39;re not the developer type, or you just don&#39;t have time,\r\nyou&#39;re always welcome to download and test the latest version\r\nand use the issue tracker here on the project page to report any\r\nissues you come across.</p>\r\n\r', 2, NULL, 1, '', '', '2012-01-03 05:47:14', '2012-02-01 00:31:39'),
(4, 'another-page', 'Another Page', 'another, page', 'Another page description', 'Another Page', 'Just Another Page', '<p>This is another page you can edit to your own needs, or delete\r\nit and create a new one.</p>\r\n\r', 2, NULL, 3, '', '', '2012-01-03 05:49:26', '2012-02-01 04:08:26'),
(5, 'hidden', 'Hidden Page', 'hidden', 'this is a hidden page', 'Hidden Page', 'Hidden Page', 'This page shouldn''t appear in the nav...', 1, NULL, 4, '', '', '2012-01-03 06:11:53', '2012-01-04 02:28:55'),
(6, 'test', 'Testing', 'test, testing, page', 'Just a test page', 'Test', 'Test Page', '<p>This is just a test page for testing... asdasdsadasdas</p>\r\n\r', 0, NULL, 5, '', '', '2012-01-03 06:39:18', '2012-01-08 21:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `prefs`
--

CREATE TABLE IF NOT EXISTS `prefs` (
  `site_name` varchar(50) NOT NULL,
  `site_slogan` varchar(100) DEFAULT NULL,
  `layout` varchar(50) NOT NULL DEFAULT 'default',
  `timezone` tinyint(5) NOT NULL DEFAULT '0',
  `copyright` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`site_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prefs`
--

INSERT INTO `prefs` (`site_name`, `site_slogan`, `layout`, `timezone`, `copyright`) VALUES
('Quake CMS', 'Earth Shattering... or Something', 'default', -5, 'Â© 2012 Chris Clower & Clowerweb                                                            \r\n            ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `privs` tinyint(3) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_failure` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_failures` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `privs`, `creation_date`, `last_login`, `last_failure`, `login_failures`, `ip`) VALUES
(1, 'admin', 'password', 1, '2011-12-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
